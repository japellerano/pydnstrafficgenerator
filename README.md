# pyDNSTrafficGenerator  

Small utility to supplement DNS traffic to bring up the floor of noise to make it more difficult for ISPs to determine operating patterns.  

## How to Use  

## Reference  

### Python DNS

- [DNS Python](https://www.dnspython.org/)
- [DNS Python Docs](https://dnspython.readthedocs.io/en/latest)

### CSV Files in Python

- [Official Python Documentation](https://docs.python.org/3/library/csv.html)

### Setup Tools

- [A Practical Guide to Using Setup.py](https://godatadriven.com/blog/a-practical-guide-to-using-setup-py/)
- [Using Python setuptools](https://christophergs.com/python/2016/12/11/python-setuptools/)
- [Packaging and distributing projects](https://packaging.python.org/guides/distributing-packages-using-setuptools/)

### Click 

- [Create Your First CLI Application With Python Click](https://betterprogramming.pub/python-click-building-your-first-command-line-interface-application-6947d5319ef7)  
- [Quickstart - Click Documentation](https://click.palletsprojects.com/en/8.0.x/quickstart/#screencast-and-examples)

### Threading 

- [An Intro to Threading in Python](https://realpython.com/intro-to-python-threading/)
- [Official Python Threading Documentation](https://docs.python.org/3/library/threading.html)
- [A Practical Guide to Python Threading](https://www.pythontutorial.net/advanced-python/python-threading/)