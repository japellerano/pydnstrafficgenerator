'''
Author: Addison Pellerano
Author Email: james@japellerano.com
'''

"""Import statements"""
from csv import DictReader
from generator.utils.gen_setup import dataExtract, getSiteList
import os

# getSiteList("https://www.domcop.com/files/top/top10milliondomains.csv.zip")
# dataExtract("domain_list.zip")

""" resolveDomain
Looks up domain names
Takes a domain name as a string domainName
Returns the response
Usage: resolveDomain('wordpress.org')
"""
def resolveDomain(domainName):
  
    response = os.system("ping -c 1 " + domainName)

    return response

""" queryList function
Takes string of csv file name and columnHeader
Usage: queryList('csv_file.csv', 'Domain')
"""
def queryList(csv, columnHeader):

    """ Import CSV file """
    with open(csv, 'r') as csvObj:

      csv_reader = DictReader(csvObj)

      # TODO: Log sites that don't work

      """ Iterate over each row of the CSV file. 
          For each row call resolveDomain().
          Pass row number and the column header name.
      """
      for row in csv_reader:

        resolveDomain(row[columnHeader])

queryList('domains.csv', "Domain")

