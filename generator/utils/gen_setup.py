'''
Author: Addison Pellerano
Author Email: james@japellerano.com
'''

import requests
import os
from zipfile import ZipFile

# define function which grabs a list in csv format
# takes a url, type string
# saves the output to dirty_list.csv
def getSiteList(url):

    # create filename, type string
    # name of zip file 
    filename = "domain_list.zip"

    # Check to see if filename exists
    # If it is true, print and then exit
    if os.path.exists(filename) == True:
      print("The file already exists!")
    # If the file does not exist
    else:
      # curl url - zip file
      r = requests.get(url)
      with open(filename, 'wb') as f:
        f.write(r.content)

# extract data from zip file
# remove zip file
# takes a the zipped file name as a string
def dataExtract(zipFileName):
    # print("Extracting zipfile")

    # TODO: Check to see if domains.csv exists
    # Use ZipFile module to pass zip file
    with ZipFile(zipFileName, 'r') as zip:
      zip.printdir()

      # Extracts zip file
      zip.extractall()

      # Gets the name of the file extracted
      f_name = zip.namelist()

      # Gets the file name and renames it
      # TODO: Fix this to look for the csv file within the name list
      os.rename(f_name[0], 'domains.csv')
      
      # Create variable of name of file
      # TODO: search data directory for filename instead of hard coded
      domains_list = 'domains.csv'

      # TODO: Remove zipfile
      # Cleanup
      # Remove zip file if the old one still exists
      os.remove(zipFileName)

      # Remove original csv if it still exists
      if os.path.exists(f_name[0]):
        os.remove(f_name[0])

      # return domains_list variable
      return domains_list



