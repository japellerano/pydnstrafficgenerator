from setuptools import setup, find_packages

setup(
    name="pyDNSTrafficGenerator",
    version='0.1.3',
    author='James Addison Pellerano',
    author_email='james@japellerano.com',
    url='https://japellerano.com/',
    packages=find_packages(),
)